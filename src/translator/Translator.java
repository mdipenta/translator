package translator;

import java.util.regex.Pattern;

public class Translator {
	protected String sentence;

	public Translator(String sentence) {
		this.sentence=sentence;
		
	}

	public String getSentence() {
		return this.sentence;
	}

	public String translate() {
		String result="";
		if(this.sentence.equals(""))
			result="nil";
		
		if(Pattern.compile("^[aeiou][a-z]*y$",Pattern.CASE_INSENSITIVE).matcher(sentence).find())
			result=sentence+"nay";
		
		return result;
	}

}
